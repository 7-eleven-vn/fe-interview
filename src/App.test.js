import { render, screen } from '@testing-library/react';
import App from './App';

test('renders App', () => {
  render(<App />);
  const linkElement = screen.getByText(/7-Eleven Việt Nam/i);
  expect(linkElement).toBeInTheDocument();
});

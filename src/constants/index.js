export const PRODUCTS = [
  {
    "id": 1,
    "name": "Bánh Sandwich Jambon Phô Mai Nướng",
    "price": 25000,
    "original_price": 30000,
    "image_url": "https://images.unsplash.com/photo-1546039907-7fa05f864c02?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80"
  },
  {
    "id": 2,
    "name": "Bánh Sừng Trâu Jambon Phô Mai",
    "price": 35000,
    "original_price": 50000,
    "image_url": "https://images.unsplash.com/photo-1604908176997-125f25cc6f3d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1515&q=80"
  },
  {
    "id": 3,
    "name": "Bánh Mì Que Pate Heo Cay",
    "price": 12000,
    "image_url": "https://images.unsplash.com/photo-1560684352-8497838a2229?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1056&q=80"
  },
  {
    "id": 4,
    "name": "Bánh Mì Trứng Ốp La",
    "price": 18000,
    "image_url": "https://images.unsplash.com/photo-1514326640560-7d063ef2aed5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80"
  },
  {
    "id": 5,
    "name": "Bánh Mì Thịt Chả",
    "price": 25000,
    "image_url": "https://images.unsplash.com/photo-1484344958632-f7264ab71ecc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1882&q=80"
  }
];

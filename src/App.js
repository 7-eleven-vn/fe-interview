import logo from './assets/images/logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <img src={logo} className="App-logo" alt="logo" />
      <h1>Welcome to 7LAB@7-Eleven VN coding challenge</h1>
      <a
        className="App-link"
        href="https://www.7-eleven.vn/"
        target="_blank"
        rel="noopener noreferrer"
      >
        7-Eleven Việt Nam
      </a>
    </div>
  );
}

export default App;

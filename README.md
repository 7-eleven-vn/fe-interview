## Welcome to 7LAB@7-Eleven VN coding challenge

_Hello!_
_We're excited that you're interested in joining the 7-Eleven VN. Below are the requirements and explanations for the challenge._

### Notes:

- Our challenge codebase is based on create-react-app.
- All provided codes are in this repository. Please fork, complete your challenge, and create a PR for us.
- We judge your codes:
  - Easy to understand.
  - Well organized.
  - Performant.
  - Test cases.
  - Your behavior when meet a new tech
- Dont worry if you cant complete the challenge in time. Just do your best in a mindful way.
- If you cant fully complete the challenge , please note the completed features.

### Figma design

[App design](https://www.figma.com/file/lAfRTdj3x82yHYd3GgG1XL/7-Eleven-Frontend-Interview?node-id=0%3A1)

### Requirements

- Create an order app based on the design (We prefer not using any CSS framework as we want to see your CSS skill).
- Please help us implement an order app by:
  - Add new product to the cart (Product list already defined in project: constants/index.js).
  - Increase/Decrease quantity, if quantity is zero, remove the product from the cart.
  - Implement the persistent feature. After refreshing, our products will be disappeared, that's annoying for our users.
- Write some tests (prefer unit tests as it can save your time).

### How to run this code

- Run `yarn` or `npm install` if this is the first time you clone this repo (`master` branch).
- Run `yarn start` to start this project in development mode.

Last updated: 2021/09/08
